# Master Degree Project - Semester 2

This project is about the method [Forest RK](https://hal.archives-ouvertes.fr/hal-00436367/document).

The aims of the study are:
1. Performance comparison between Forest-RI algorithm with several values of parameter `k` and Forest-RK algorithm, on several datasets
2. Analysis of the influence of dataset features importance and Forest-RK algorithm

It contains the followings sections :
- [Codes](codes/): store all codes used to run experiments on Forest RK method (method and benchmarks)
- [Results](results/): store all results of the experiments
- [Visualisations](visualisations/): store visualisations about features importances analysis and max features parameter influence
